FROM python:3.9
COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt

ENV HOME /home/user
RUN useradd --create-home --home-dir $HOME user \
    && chmod -R u+rwx $HOME \
    && chown -R user:user $HOME

RUN mkdir /shared_directory_images && chown user:user /shared_directory_images

USER user

COPY --chown=user:user main.py utils.py cgv_connector_pb2.py /app/
COPY --chown=user:user config/ /app/config/
WORKDIR "/app"
ENTRYPOINT "python" "main.py"
