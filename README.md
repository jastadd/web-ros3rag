# Dashboard for ros3rag use case

[Java impl repo](https://git-st.inf.tu-dresden.de/jastadd/ros3rag) | [Paper repo](https://git-st.inf.tu-dresden.de/stgroup/publications/2021/ragconnect-followup) | [Paper issue](https://git-st.inf.tu-dresden.de/stgroup/publications/kanban/2021/-/issues/3)

## Running

- Install requirements, best using virtual environment
- Run `python main.py`
- Open <http://127.0.0.1:8050/>
